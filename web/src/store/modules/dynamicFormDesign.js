import {
  getToken,
  setToken,
  removeToken
} from '@/utils/auth'
import {
  resetRouter
} from '@/router'
import {
  MessageBox,
  Message
} from 'element-ui'
import request from '@/utils/request'


const getDefaultState = () => {
  return {

  }
}

const state = getDefaultState()

const mutations = {

}

const actions = {
  create({
    commit,
    state
  }, data) {
    return request({
      url: '/api/FormDesign/Create',
      method: 'post',
      data: data
    })
  },
  update({
    commit,
    state
  }, data) {
    return request({
      url: '/api/FormDesign/Update',
      method: 'put',
      data: data
    })
  },
  get({
    commit,
    state
  }, params) {
    return request({
      url: '/api/FormDesign/Get',
      method: 'get',
      params: params
    })
  },
  getFormDesignVersion({
    commit,
    state
  }, params) {
    return request({
      url: '/api/FormDesign/GetFormDesignVersion',
      method: 'get',
      params: params
    })
  },
  delete({
    commit,
    state
  }, params) {
    return request({
      url: '/api/FormDesign/Delete',
      method: 'delete',
      params: params
    })
  },
  getAllFormDesignVersions({
    commit,
    state
  }, params) {
    return request({
      url: '/api/FormDesign/getAllFormDesignVersions',
      method: 'get',
      params: params
    })
  },
  getAllList({
    commit,
    state
  }, params) {
    return request({
      url: '/api/FormDesign/getAllList',
      method: 'get',
      params: params
    })
  },
  setActiveVersion({
    commit,
    state
  }, data) {
    return request({
      url: '/api/FormDesign/SetActiveVersion',
      method: 'put',
      data: data,
      params: {
        id: data.id
      }
    })
  },
  getPageList({
    commit,
    state
  }, params) {
    return request({
      url: '/api/FormDesign/GetPageList',
      method: 'get',
      params: params
    })
  },
  getFormDesignVersionByWorkflowId({
    commit,
    state
  }, params) {
    return request({
      url: '/api/FormDesign/GetFormDesignVersionByWorkflowId',
      method: 'get',
      params: params
    })
  },
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
