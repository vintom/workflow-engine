import {
  getToken,
  setToken,
  removeToken,
  getCurrentUser,
  setCurrentUser,
} from "@/utils/auth";
import { resetRouter } from "@/router";
import { MessageBox, Message } from "element-ui";
import request from "@/utils/request";

const getDefaultState = () => {
  return {
    token: getToken(),
    name: "",
    avatar: "",
    user: getCurrentUser() || {},
  };
};

const state = getDefaultState();

const mutations = {
  RESET_STATE: (state) => {
    Object.assign(state, getDefaultState());
  },
  SET_TOKEN: (state, token) => {
    state.token = token;
  },
  SET_NAME: (state, name) => {
    state.name = name;
  },
  SET_AVATAR: (state, avatar) => {
    state.avatar = avatar;
  },
  SET_CURRENTUSER: (state, user) => {
    state.user = user;
  },
};

const actions = {
  // user login
  login({ commit }, userInfo) {
    const { username, password } = userInfo;
    return new Promise((resolve, reject) => {
      return request({
        url: "/api/Account/Login",
        method: "post",
        data: { uid: username, pwd: password },
      })
        .then((res) => {
          commit("SET_TOKEN", res);
          setToken(res.token);
          setCurrentUser(res.userInfo);
          commit("SET_CURRENTUSER", res.userInfo);
          resolve();
          return res;
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  autoLogin({ commit }, userInfo) {
    const { username, password } = userInfo;
    return new Promise((resolve, reject) => {
      return request({
        url: "/api/Account/autoLogin?access_token=" + userInfo.access_token,
        method: "post",
        data: { uid: username, pwd: password },
      })
        .then((res) => {
          commit("SET_TOKEN", res);
          setToken(res.token);
          setCurrentUser(res.userInfo);
          commit("SET_CURRENTUSER", res.userInfo);
          resolve();
          return res;
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  // get user info
  // getInfo({ commit, state }) {
  //   return new Promise((resolve, reject) => {
  //     commit("SET_NAME", "admin");
  //     commit(
  //       "SET_AVATAR",
  //       "https://himg.bdimg.com/sys/portraitn/item/942f5139303436cd06"
  //     );
  //     resolve({});

  //     // getInfo(state.token).then(response => {
  //     //   const { data } = response

  //     //   if (!data) {
  //     //     return reject('Verification failed, please Login again.')
  //     //   }

  //     //   const { name, avatar } = data

  //     //   commit('SET_NAME', name)
  //     //   commit('SET_AVATAR', avatar)
  //     //   resolve(data)
  //     // }).catch(error => {
  //     //   reject(error)
  //     // })
  //   });
  // },

  // user logout
  logout({ commit, state }) {
    return new Promise((resolve, reject) => {
      return request({
        url: "/api/Account/Logout",
        method: "post",
      })
        .then(() => {
          removeToken(); // must remove  token  first
          resetRouter();
          commit("RESET_STATE");
          resolve();
        })
        .catch((error) => {
          removeToken(); // must remove  token  first
          resetRouter();
          commit("RESET_STATE");
          resolve();
        });
    });
  },
  getInfo({ commit, state }) {
    return new Promise((resolve, reject) => {
      return request({
        url: "/api/Account/GetUserInfo",
        method: "get",
        data: {},
      })
        .then((res) => {
          setCurrentUser(res);
          commit("SET_CURRENTUSER", res);
          commit(
            "SET_AVATAR",
            "https://himg.bdimg.com/sys/portraitn/item/942f5139303436cd06"
          );
          resetRouter();
          resolve();
          return res;
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  // remove token
  resetToken({ commit }) {
    return new Promise((resolve) => {
      removeToken(); // must remove  token  first
      commit("RESET_STATE");
      resolve();
    });
  },

  setCurrentUser({ commit, state }, data) {
    state.user = data;
    setCurrentUser(data);
  },

  getCurrentUser({ commit, state }, data) {
    return getCurrentUser();
  },

  getAllList({ commit, state }, params) {
    return request({
      url: "/api/Account/GetAllList",
      method: "get",
      params: params,
    }).then((res) => {
      if (!getCurrentUser() && res) {
        commit("SET_CURRENTUSER", res[0]);
        setCurrentUser(res[0]);
      }

      return res;
    });
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
