import Cookies from 'js-cookie'

const TokenKey = 'vue_admin_template_token'
const ThirdpartTokenKey = 'Thirdpart_vue_admin_template_token'
const UserKey = 'vue_admin_template_user'

export function getToken() {
  return Cookies.get(TokenKey)
}

export function setToken(token) {
  return Cookies.set(TokenKey, token)
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}

export function setCurrentUser(user) {
  return localStorage.setItem(UserKey, JSON.stringify(user))
}
export function getCurrentUser() {
  try{
    return JSON.parse(localStorage.getItem(UserKey))
  }catch(e){
    return null
  }
}

export function getThirdpartToken() {
  return Cookies.get(ThirdpartTokenKey)
}

export function setThirdpartToken(token) {
  return Cookies.set(ThirdpartTokenKey, token)
}

