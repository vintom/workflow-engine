import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

/* Layout */
import Layout from "@/layout";
import MyLayout from "@/layout/MyLayout";

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'/'el-icon-x' the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: "/login",
    component: () => import("@/views/login/index"),
    hidden: true,
  },

  {
    path: "/404",
    component: () => import("@/views/404"),
    hidden: true,
  },

  {
    path: "/",
    component: Layout,
    redirect: "/workflow/myFormInstance",
    // children: [{
    //   path: 'dashboard',
    //   name: 'Dashboard',
    //   component: () => import('@/views/dashboard/index'),
    //   meta: {
    //     title: '工作台',
    //     icon: 'dashboard'
    //   }
    // }]
  },

  {
    requiredManager: true,
    path: "/workflowDesign",
    component: Layout,
    meta: {
      title: "流程设计",
      icon: "el-icon-bangzhu",
    },
    children: [
      {
        path: "list",
        name: "workflow",
        component: () => import("@/views/workflows/workflow/index"),
        meta: {
          title: "流程设计",
          icon: "el-icon-document",
        },
      },
      {
        path: "simulation",
        name: "simulation",
        hidden: true,
        component: () => import("@/views/workflows/workFlowSimulation/index"),
        meta: {
          title: "流程模拟",
          icon: "dashboard",
        },
      },
      {
        path: "formList",
        name: "formList",
        component: () => import("@/views/workflows/formDesign/index"),
        meta: {
          title: "流程表单",
          icon: "el-icon-tickets",
        },
      },
      {
        path: "editPage",
        name: "editPage",
        hidden: true,
        component: () => import("@/views/workflows/formDesign/editPage"),
        meta: {
          title: "表单设计",
          icon: "el-icon-tickets",
        },
      },
    ],
  },

  {
    path: "/workflow",
    component: Layout,
    meta: {
      title: "流程审批",
      icon: "el-icon-bangzhu",
    },
    children: [
      {
        path: "formInstance",
        name: "formInstance",
        component: () => import("@/views/workflows/formInstance/index"),
        meta: {
          title: "所有审批",
          icon: "el-icon-document-checked",
        },
        requiredManager: true,
        // hidden:true,
      },
      {
        path: "myFormInstance",
        name: "myFormInstance",
        component: () => import("@/views/workflows/myFormInstance/index"),
        meta: {
          title: "我发起的",
          icon: "el-icon-document-checked",
        },
      },
      {
        path: "handledInstance",
        name: "handledInstance",
        component: () => import("@/views/workflows/pendings/handledIndex"),
        meta: {
          title: "我处理的",
          icon: "el-icon-document-checked",
        },
      },
      {
        path: "pendings",
        name: "pendings",
        component: () => import("@/views/workflows/pendings/index"),
        meta: {
          title: "待办流程",
          icon: "el-icon-bell",
        },
      },

      {
        path: "list",
        name: "list",
        hidden: true,
        redirect: "/workflowDesign/list",
      },
    ],
  },

  {
    path: "/editworkflow",
    component: MyLayout,
    name: "editworkflow",
    hidden: true,
    meta: {
      title: "editworkflow",
      icon: "el-icon-s-help",
    },
    children: [
      {
        path: "edit",
        name: "editworkflow",
        meta: {
          title: "editworkflow",
          icon: "el-icon-s-help",
        },
        component: () => import("@/views/workflows/workflow/edit"),
      },
    ],
  },

  // // {
  // //   path: '/editscreen',
  // //   component: MyLayout,
  // //   name: 'Screen',
  // //   hidden:true,
  // //   meta: { title: 'Screen', icon: 'el-icon-s-help' },
  // //   children: [
  // //     {
  // //       path: 'edit',
  // //       name: 'editscreen',
  // //       meta: { title: 'Screen', icon: 'el-icon-s-help' },
  // //       component: () => import('@/views/screen/edit'),
  // //     },
  // //   ]
  // // },

  // {
  //   path: '/tools',
  //   component: Layout,
  //   children: [{
  //     path: 'swagger2api',
  //     name: 'swagger2api',
  //     component: () => import('@/views/tools/swagger2api/index'),
  //     meta: {
  //       title: '接口生成',
  //       icon: 'el-icon-s-cooperation'
  //     }
  //   }, ]
  // },

  // {
  //   path: '/organisations',
  //   name: 'organisations',
  //   component: Layout,
  //   meta: {
  //     title: '组织用户',
  //     icon: 'el-icon-user'
  //   },
  //   children: [{
  //       path: 'user',
  //       name: 'user',
  //       component: () => import('@/views/organisation/user/index'),
  //       meta: {
  //         title: '用户',
  //         icon: 'el-icon-user'
  //       }
  //     },
  //     {
  //       path: 'role',
  //       name: 'role',
  //       component: () => import('@/views/organisation/role/index'),
  //       meta: {
  //         title: '角色',
  //         icon: 'el-icon-s-custom'
  //       }
  //     },
  //     {
  //       path: 'org',
  //       name: 'org',
  //       component: () => import('@/views/organisation/org/index'),
  //       meta: {
  //         title: '组织',
  //         icon: 'el-icon-s-platform'
  //       }
  //     },
  //   ]
  // },

  {
    path: "/plugins",
    component: Layout,
    meta: {
      title: "插件配置",
      icon: "el-icon-bangzhu",
    },
    children: [
      {
        path: "list",
        name: "plugins",
        component: () => import("@/views/plugins/index"),
        meta: {
          title: "插件配置",
          icon: "el-icon-document",
        },
      },
    ],
    // hidden:true,
    requiredManager: true,
  },

  // 404 page must be placed at the end !!!
  {
    path: "*",
    redirect: "/404",
    hidden: true,
  },
];

let _routes=[];

export function getManagerRouters() {
  if(_routes.length>0)
  {
    return _routes;
  }

  let routers = [];

  constantRoutes.forEach((route) => {
    if (route.requiredManager) {
      routers.push(route.path);
    }

    if (route.children && route.children.length > 0) {
      route.children.forEach((child) => {
        if (route.requiredManager || child.requiredManager) {
          routers.push(route.path+'/'+child.path );
        }
      });
    }
  });
  _routes = routers;
  return _routes;
};

const createRouter = () =>
  new Router({
    // mode: 'history', // require service support
    scrollBehavior: () => ({
      y: 0,
    }),
    routes: constantRoutes,
  });

const router = createRouter();

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter();
  router.matcher = newRouter.matcher; // reset router
}

export default router;
