﻿using System;
using System.Collections.Generic;
using System.Text;
using WorkFlowCore.Conditions;

namespace Condition_PluginDemo
{
    [Condition("条件处理器插件demo", "这只是一个demo,当传入参数（parameter）为 'true'（包括引号） 时通过")]
    public class ConditionDemo : ICondition
    {
        public bool CanAccept(ConditionInput input)
        {
            return "true".Equals(input.Expression);
        }
    }
}
