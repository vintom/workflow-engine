﻿namespace WorkFlowCore.Host.ViewModels.Account
{
    public class LoginOutput
    {
        public string token { get; set; }
        public Common.Authorization.JWT.AuthorizationUser userInfo { get; set; }
    }
}
