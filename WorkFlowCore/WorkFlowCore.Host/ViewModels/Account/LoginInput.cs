﻿using System.ComponentModel.DataAnnotations;

namespace WorkFlowCore.Host.ViewModels.Account
{
    public class LoginInput
    {
        [Required]
        public string uid { get; set; }
        [Required]
        public string pwd { get; set; }
    }
}
