﻿namespace WorkFlowCore.Host.ViewModels.WorkFlowCore
{
    public class GetUsersOfUserSelectonInput
    {
        public string userSelectorId { get; set; }
        public string selectionIds { get; set; }
        public string[] InputSelectionIds { get => selectionIds?.Split(',')??new string[0]; }
    }
}
