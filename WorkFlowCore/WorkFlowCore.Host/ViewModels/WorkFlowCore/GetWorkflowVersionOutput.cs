﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System;
using WorkFlowCore.Workflows;

namespace WorkFlowCore.Host.ViewModels.WorkFlowCore
{
    public class GetWorkflowVersionOutput
    {
        public Guid Id { get; set; }
        /// <summary>
        /// 流程编号
        /// </summary>
        [Required]
        public Guid WorkflowId { get; set; }
        /// <summary>
        /// 版本号
        /// </summary>
        [Required]
        public int VersionNo { get; set; }
        /// <summary>
        /// 绘制信息，前端绘制所需信息
        /// </summary>
        public string DrawingInfo { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        [StringLength(2000)]
        public string Description { get; set; }
        /// <summary>
        /// 节点映射信息
        /// </summary>
        public List<NodeMap> NodeMaps { get; set; }

        public List<WorkflowNode> AllNodes { get; set; }
    }
}
