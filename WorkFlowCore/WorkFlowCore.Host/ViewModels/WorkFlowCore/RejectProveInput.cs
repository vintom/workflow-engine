﻿using System.Collections.Generic;
using System;

namespace WorkFlowCore.Host.ViewModels.WorkFlowCore
{
    public class RejectProveInput: ProveInput
    {
        /// <summary>
        /// 指定驳回节点id
        /// </summary>
        public Guid? RejectToNodeId { get; set; }
    }
}
