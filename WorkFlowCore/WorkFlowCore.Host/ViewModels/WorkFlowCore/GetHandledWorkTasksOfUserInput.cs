﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WorkFlowCore.Host.ViewModels.WorkFlowCore
{
    public class GetHandledWorkTasksOfUserInput
    {
        public int CurrentPage { get; set; } = 1;
        public int MaxResultCount { get; set; } = -1;
    }

}
