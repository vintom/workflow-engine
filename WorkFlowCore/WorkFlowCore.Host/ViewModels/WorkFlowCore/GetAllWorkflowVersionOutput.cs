﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WorkFlowCore.Host.ViewModels.WorkFlowCore
{
    public class GetAllWorkflowVersionOutput
    {
        /// <summary>
        /// 流程编号
        /// </summary>
        public Guid WorkflowId { get; set; }
        /// <summary>
        /// 版本号
        /// </summary>
        public int VersionNo { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }

        public DateTime ModifiedTime { get; set; }
        public DateTime CreationTime { get; set; }
    }
}
