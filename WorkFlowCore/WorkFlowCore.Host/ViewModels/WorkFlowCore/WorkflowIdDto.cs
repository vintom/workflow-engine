﻿using System;
using System.Collections.Generic;
using System.Text;
using WorkFlowCore.Workflows;

namespace WorkFlowCore.Host.ViewModels.WorkFlowCore
{
    public class WorkflowIdDto: WorkflowId
    {
        /// <summary>
        /// 流程名称
        /// </summary>
        public string WorkflowName { get; set; }
    }
}
