﻿using System;

namespace WorkFlowCore.Host.ViewModels.WorkFlowCore
{
    public class UpdateWorkTaskFormDataInput
    {
        public Guid  Id { get; set; }
        /// <summary>
        /// 表单数据（json）
        /// </summary>
        public string FormData { get; set; }
    }
}
