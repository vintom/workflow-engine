﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WorkFlowCore.Host.ViewModels.WorkFlowCore
{
    public class CreateWorkFlowInput
    {
        public string Name { get; set; }
        public string Des { get; set; }
    }
}
