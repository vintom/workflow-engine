﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WorkFlowCore.Host.ViewModels.WorkFlowCore
{
    public class GetAllWorkflowInput
    {
        public int CurrentPage { get; set; } = 1;
        public int SkipCount { get => (CurrentPage - 1) *MaxResultCount; }

        public int MaxResultCount { get; set; } = -1;
    }
}
