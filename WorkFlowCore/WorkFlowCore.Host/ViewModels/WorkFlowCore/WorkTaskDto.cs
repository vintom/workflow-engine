﻿using System;
using System.Collections.Generic;
using System.Text;
using WorkFlowCore.IRepositories;
using WorkFlowCore.Workflows;
using WorkFlowCore.WorkTasks;

namespace WorkFlowCore.Host.ViewModels.WorkFlowCore
{
    /// <summary>
    /// 工作流dto
    /// </summary>
    public class WorkTaskDto: WithBaseInfoEntity
    {
        /// <summary>
        /// 流程id
        /// </summary>
        public WorkflowIdDto WorkflowId { get; set; }
        /// <summary>
        /// 任务名称
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// 表单数据（json）
        /// </summary>
        public string FormData { get; set; }
        /// <summary>
        /// 实体全称
        /// </summary>
        public string EntityFullName { get; set; }
        /// <summary>
        /// 实体主键值
        /// </summary>
        public string EntityKeyValue { get; set; }
        /// <summary>
        /// 审批状态
        /// </summary>
        public WorkTaskStatus WorkTaskStatus { get; set; } = WorkTaskStatus.Pending;

        public bool IsProcessed { get => WorkTaskStatus == WorkTaskStatus.Processed; }
        public bool IsPending { get => WorkTaskStatus == WorkTaskStatus.Pending; }
        public bool IsProcessing { get => WorkTaskStatus == WorkTaskStatus.Processing; }
        public bool IsSimulation { get; set; }
    }
}
