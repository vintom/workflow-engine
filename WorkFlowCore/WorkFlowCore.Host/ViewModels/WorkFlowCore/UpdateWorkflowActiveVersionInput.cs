﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WorkFlowCore.Host.ViewModels.WorkFlowCore
{
    public class UpdateWorkflowActiveVersionInput
    {
        public Guid WorkflowId { get; set; }
        public int ActiveVersion { get; set; }
    }
}
