﻿using System.Reflection;
using WorkFlowCore.Plugins;

namespace WorkFlowCore.Host.ViewModels.Plugins
{
    public class ManifestWithConfigDto
    {
        public string Entry { get; set; }
        public string Version { get; set; }
        /// <summary>
        /// 类型名称
        /// </summary>
        public string ClassName { get; set; }
        /// <summary>
        /// 插件名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }
        public bool Configurable { get; set; }
    }

    public class ParameterConfigDto
    {
        public string Label { get; set; }
        public string KeyName { get; set; }
        public string DefaultValue { get; set; }
        public string Description { get; set; }
    }
}
