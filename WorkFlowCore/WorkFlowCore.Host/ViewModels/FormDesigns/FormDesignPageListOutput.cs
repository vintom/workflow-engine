﻿using System;
using System.Collections.Generic;
using System.Text;
using WorkFlowCore.FormDesigns;

namespace WorkFlowCore.Host.ViewModels.FormDesigns
{
    public class FormDesignPageListOutput: FormDesign
    {
        /// <summary>
        /// 流程名称
        /// </summary>
        public string WorkflowName { get; set; }
    }
}
