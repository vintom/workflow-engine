﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WorkFlowCore.Host.ViewModels
{
    public class OutputDto : OutputDto<object>
    {
        
        public static OutputDto<DataType> Succeed<DataType>(DataType data)
        {
            return new OutputDto<DataType>
            {
                Code = "success",
                Msg = "成功",
                Data = data
            };
        }
        public static OutputDto<DataType> Failed<DataType>(string msg)
        {
            return new OutputDto<DataType>
            {
                Code = "fail",
                Msg = msg,
                Data = default(DataType)
            };
        }

        public static OutputDto<DataType> Result<DataType>(bool  isSucced , DataType data,string msg="")
        {
            return isSucced ? Succeed(data) : Failed<DataType>(msg);
        }
    }
    public class OutputDto<DataType>
    {
        public string Code { get; set; }
        public string Msg { get; set; }
        public DataType Data { get; set; }

        
    }
}
