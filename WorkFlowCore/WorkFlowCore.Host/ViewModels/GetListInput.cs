﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkFlowCore.Host.ViewModels
{
    public class GetListInput
    {
        public string Sorting { get; set; }
    }
}
