﻿using Quartz;
using System.Threading.Tasks;
using Volo.Abp.BackgroundWorkers.Quartz;
using WorkFlowCore.Workflows;

namespace WorkFlowCore.Host.BackgroundWorkers
{
    public class AutoClearEmptyWorkflowsBackgroundWorker : QuartzBackgroundWorkerBase
    {
        private readonly WorkflowManager workflowManager;

        public AutoClearEmptyWorkflowsBackgroundWorker(WorkflowManager workflowManager)
        {
            JobDetail = JobBuilder.Create<AutoClearEmptyWorkflowsBackgroundWorker>().WithIdentity(nameof(AutoClearEmptyWorkflowsBackgroundWorker)).Build();
            Trigger = TriggerBuilder.Create().WithIdentity(nameof(AutoClearEmptyWorkflowsBackgroundWorker)).WithCronSchedule("0 0 */2 * * ?").Build();
            this.workflowManager = workflowManager;
        }

        public override Task Execute(IJobExecutionContext context)
        {
            return workflowManager.ClearEmptyWorkflows();
            //return Task.CompletedTask;
        }
    }
}
