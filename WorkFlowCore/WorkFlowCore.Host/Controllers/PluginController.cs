﻿using Mapster;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Volo.Abp.AspNetCore.Mvc;
using WorkFlowCore.Authorization;
using WorkFlowCore.Common.Authentication.JWT;
using WorkFlowCore.Host.ViewModels.Account;
using WorkFlowCore.Host.ViewModels.Plugins;
using WorkFlowCore.Plugins;

namespace WorkFlowCore.Host.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class PluginController : AbpControllerBase
    {
        private readonly PluginManager pluginManager;
        private readonly IWorkflowSession workflowSession;

        public PluginController(PluginManager pluginManager, IWorkflowSession workflowSession)
        {
            this.pluginManager = pluginManager;
            this.workflowSession = workflowSession;
        }

        [HttpGet("GetPluginApplyConfigs")]
        public async Task<PluginApplyConfigDto> GetPluginApplyConfigs()
        {
            if (!workflowSession.IsManager) return default;
            var configs =await pluginManager.GetPluginApplyConfigs();
            var manifests = pluginManager.GetPluginManifests();

            var result = PluginApplyConfigDto.FromPluginApplyConfig(configs.ToList(), pluginManager.GetPluginTypes(), manifests);

            return result;
        }

        [HttpGet("GetManifestWithConfigs")]
        public async Task<ManifestWithConfigDto[]> GetPluginManifests([FromQuery]string executableTypeFullName)
        {
            if (!workflowSession.IsManager) return default;
            var result = pluginManager.GetPluginManifests(executableTypeFullName).ToArray();
            return result.Adapt<ManifestWithConfigDto[]>();
        }

        [HttpPost("AddOrUpdateConfig")]
        public async Task AddOrUpdateConfig([FromBody]PluginApplyConfigDto pluginApplyConfigDto)
        {
            if (!workflowSession.IsManager) return;
            var configs = pluginApplyConfigDto.ToPluginApplyConfig();
            await pluginManager.AddOrUpdateConfig(configs);
        }
    }
}
