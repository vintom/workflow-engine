﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using Volo.Abp.AspNetCore.Mvc;
using WorkFlowCore.FormDesigns;
using WorkFlowCore.IRepositories;
using WorkFlowCore.Host.ViewModels.FormDesigns;
using WorkFlowCore.Workflows;
using WorkFlowCore.Host.ViewModels;
using Volo.Abp.Application.Dtos;
using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace WorkFlowCore.Host.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class FormDesignController : AbpControllerBase
    {
        private readonly FormDesignManager formDesignManager;
        private readonly IBasicRepository<FormDesignInfo, Guid> repository;
        private readonly IBasicRepository<WorkflowVersionInfo> workflowVersionRepository;
        private readonly IBasicRepository<Workflow> workflowRepository;

        public FormDesignController(FormDesignManager formDesignManager, IBasicRepository<FormDesignInfo, Guid> repository, IBasicRepository<WorkflowVersionInfo> workflowVersionRepository, IBasicRepository<Workflow> workflowRepository)
        {
            this.formDesignManager = formDesignManager;
            this.repository = repository;
            this.workflowVersionRepository = workflowVersionRepository;
            this.workflowRepository = workflowRepository;
        }
        [HttpPost("Create")]
        public async Task<FormDesign> CreateAsync(FormDesignCreateInput input)
        {
            FormDesign formDesign = await formDesignManager.CreateFormDesign(input.formType, input.title, input.description,input.workflowId);
            return formDesign;
        }
        [HttpPut("Update")]
        public async Task<FormDesignVersion> UpdateAsync(UpdateFormDesignVersionInput input)
        {
            var version = await formDesignManager.UpdateFormDesignVersion(input.formDesignId, input.formType, input.title, input.description,input.designContent,input.workflowId);
            return version;
        }
        [HttpGet("Get")]
        public async Task<FormDesign> GetAsync([FromQuery] Guid id)
        {
            var design = await repository.GetAsync(id);
            return design.ToFormDesign();
        }
        [HttpGet("GetFormDesignVersion")]
        public async Task<FormDesignVersion> GetFormDesignVersionAsync([FromQuery] FormDesignId formDesignId)
        {
            var version = await formDesignManager.GetFormDesignVersion(formDesignId);
            return version;
        }

        [HttpGet("GetFormDesignVersionByWorkflowId")]
        public async Task<FormDesignVersion> GetFormDesignVersionByWorkflowIdAsync([FromQuery] FormDesigns.WorkflowId4FormDesign workflowId)
        {
            var version = await formDesignManager.GetFormDesignActiveVersion(workflowId);
            return version;
        }


        [HttpDelete("Delete")]
        public async Task Delete(Guid id)
        {
            await formDesignManager.DeleteFormDesign(id);
        }
        [HttpGet("GetAllFormDesignVersions")]
        public async Task<List<FormDesign>> GetAllFormDesignVersionsAsync([FromQuery] Guid formDesignId)
        {
            var formDesigns = await formDesignManager.GetAllFormDesignVersions(formDesignId);
            return formDesigns;
        }
        [HttpGet("GetAllList")]
        public async Task<List<FormDesign>> GetAllListAsync([FromQuery] GetListInput input)
        {
            var entities = await repository.GetListAsync();
            var items = entities.OrderByDescending(e=>e.ModifiedTime).Select(e => e.ToFormDesign());
            return items.ToList();
        }
        [HttpPut("SetActiveVersion")]
        public async Task SetActiveVersion(FormDesignId id)
        {
            await formDesignManager.SetActiveVersion(id);
        }
        [HttpGet("GetPageList")]
        public async Task<PageResult<FormDesignPageListOutput>> GetPageListAsync([FromQuery] PagedResultRequestDto input)
        {
            var count = await repository.GetCountAsync();
            var entities = await repository.GetPagedListAsync( input.SkipCount, input.MaxResultCount, "CreationTime desc");

            var workflowIds = entities.Select(e => e.WorkflowId_Id);
            var workflowInfos = await workflowRepository.GetListAsync(w => workflowIds.Contains(w.Id));

            var workflowInfoNames = workflowInfos.ToDictionary(w => w.Id, w => w.Name);

            var result = entities.Select(e => e.ToFormDesign()).Select(entity => new FormDesignPageListOutput
            {
                Description = entity.Description,
                WorkflowName = workflowInfoNames.ContainsKey(entity.WorkflowId.Id) ? workflowInfoNames[entity.WorkflowId.Id] : "",
                ActiveVersion = entity.ActiveVersion,
                CreatedUserId = entity.CreatedUserId,
                CreationTime = entity.CreationTime,
                FormType = entity.FormType,
                Id = entity.Id,
                ModifiedTime = entity.ModifiedTime,
                Title = entity.Title,
                WorkflowId = entity.WorkflowId
            }).ToList();

            var items = entities.Select(e => e.ToFormDesign());
            return PageResult<FormDesignPageListOutput>.Result(result, count);
        }

    }
}
