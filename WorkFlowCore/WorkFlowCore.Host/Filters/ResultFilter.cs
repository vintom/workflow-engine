﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using WorkFlowCore.Host.ViewModels;

namespace WorkFlowCore.Host.Filters
{
    public class ResultFilter : IResultFilter
    {
        public void OnResultExecuted(ResultExecutedContext context)
        {

        }

        public void OnResultExecuting(ResultExecutingContext context)
        {
            if (context.Result is EmptyResult)
            {
                context.HttpContext.Response.StatusCode = 200;
                context.Result = new ObjectResult(OutputDto.Succeed(""));
            }
            else
            {
                ObjectResult originalResult = (ObjectResult)context.Result;
                context.Result = new ObjectResult(OutputDto.Succeed(originalResult.Value));
            }
            
        }
    }
}
