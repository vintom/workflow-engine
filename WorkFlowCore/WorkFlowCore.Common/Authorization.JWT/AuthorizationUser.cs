﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkFlowCore.Common.Authorization.JWT
{
    public class AuthorizationUser
    {
        public string Name { get; set;}
        public string Id { get; set; }
        public bool IsManager { get; set; }
    }
}
