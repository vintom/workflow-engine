﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkFlowCore.Common.Authorization.JWT
{
    public class NullCustomizationVerify : ICustomizationVerify
    {
        public VerifyOutput Verify(VerifyInput input)
        {
            return new VerifyOutput { IsValid = false };
        }
    }
}
