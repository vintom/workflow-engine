﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkFlowCore.Common.Authentication.JWT
{
    public class JWTAuthenticationOptions
    {
        private string _SecretKey { get; set; }
        public string SecretKey { get=>String.IsNullOrEmpty(_SecretKey)?DefaultSecretKey:_SecretKey; set => _SecretKey = value; }

        public const string DefaultSecretKey = "DA154B6B-964A-4B70-8E84-50F91EC45572";
        public const string ClaimTypesRole_Visitor="visitor";
        public const string ClaimTypesRole_Admin= "admin";
        public const string JwtClaimTypesIssuer= "WorkFlowCore";
        public const string JwtClaimTypesAudience = "user";
        public const string DefaultUid = "admin";
        public const string DefaultPwd = "admin12345.";
        public const bool AuthenticationEnacted =true; //启用认证则改为true


    }
}
