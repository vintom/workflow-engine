﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace WorkFlowCore.Common.SimplePluginLoaders
{
    public class Manifest
    {
        public string Entry { get; set; }   
        public string Version { get; set; }
        public Assembly Assembly { get; set; }
    }
}
