﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkFlowCore.UserSelectors
{
    public interface IUserSelector
    {
        /// <summary>
        /// 根据条件获取处理的用户
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        List<User> GetUsers(SelectorInput input);
        /// <summary>
        /// 获取选择的资源来源，即，前台可以从这个资源里选取数据(不一定从这里获取源数据)
        /// </summary>
        /// <returns></returns>
        List<Selection> GetSelections();
    }
}
