﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkFlowCore.UserSelectors
{
    public interface IDefaultUserSelector: IUserSelector
    {
        List<string> GetDefaultSelectionIds();
    }
}
