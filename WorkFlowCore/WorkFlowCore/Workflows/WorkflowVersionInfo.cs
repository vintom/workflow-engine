﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using WorkFlowCore.IRepositories;

namespace WorkFlowCore.Workflows
{
    public class WorkflowVersionInfo : WithBaseInfoEntity
    {

        
        /// <summary>
        /// 流程编号
        /// </summary>
        [Required]
        public Guid WorkflowId { get; set; }
        /// <summary>
        /// 版本号
        /// </summary>
        [Required]
        public int VersionNo { get; set; }
        /// <summary>
        /// 绘制信息，前端绘制所需信息
        /// </summary>
        public string DrawingInfo { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        [StringLength(2000)]
        public string Description { get; set; }
        /// <summary>
        /// 节点映射信息
        /// </summary>
        public string NodeMaps { get; set; }

        public string AllNodes { get; set; }
        public bool IsLock { get; set; }

    }
}
