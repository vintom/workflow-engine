﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using WorkFlowCore.Conditions;
using WorkFlowCore.IRepositories;

namespace WorkFlowCore.Workflows
{
    public class WorkflowLine
    {
        public WorkflowLine()
        {
        }

        public WorkflowLine(string name, WorkflowId workflowId, Guid fromNodeId, Guid toNodeId, List<LineCondition> conditions)
        {
            Name = name;
            WorkflowId = workflowId;
            FromNodeId = fromNodeId;
            ToNodeId = toNodeId;
            Conditions = conditions;
        }

        

        public void Update(string name,  Guid fromNodeId, Guid toNodeId, List<LineCondition> conditions)
        {
            Name = name;
            FromNodeId = fromNodeId;
            ToNodeId = toNodeId;
            Conditions = conditions;
        }


        public WorkflowId WorkflowId { get; set; }
        public string Name { get; set; }
        public Guid FromNodeId { get; set; }
        public Guid ToNodeId { get; set; }

        /// <summary>
        /// 绘制信息，前端绘制所需信息
        /// </summary>
        public string DrawingInfo { get; set; }
        public List<LineCondition> Conditions { get; set; } 

        
    }
}
