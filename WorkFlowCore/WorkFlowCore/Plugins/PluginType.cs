﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkFlowCore.Plugins
{
    public class PluginType
    {
        public Type ExecutableType { get; set; }
        public string ExecutableTypeFullName { get => ExecutableType?.FullName; }
        public string Name { get; set; }
    }
}
