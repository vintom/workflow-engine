﻿using System;
using System.Collections.Generic;
using System.Text;
using WorkFlowCore.UserSelectors;

namespace WorkFlowCore.Authorization
{
    /// <summary>
    /// 空session ，如果未自定义session，则使用空session
    /// </summary>
    public class EmptySession : IWorkflowSession
    {
        public EmptySession()
        {
            this.User = new User();
        }

        public User User { get; set; }

        public bool IsManager => false;
    }
}
