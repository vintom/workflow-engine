﻿using System.Collections.Generic;
using System.Text;
using WorkFlowCore.Common.EventBus;

namespace WorkFlowCore.EventData
{
    /// <summary>
    /// 自动处理节点事件
    /// </summary>
    public class AutoHandleStepsEventData : BaseEventData
    {
        public List<WorkTasks.WorkStep> Steps { get; set; }
        public string Comment { get; set; }
    }
}
