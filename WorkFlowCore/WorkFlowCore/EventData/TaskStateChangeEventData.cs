﻿using WorkFlowCore.Common.EventBus;
using System;
using System.Collections.Generic;
using System.Text;
using WorkFlowCore.WorkTasks;

namespace WorkFlowCore.EventData
{
    public class TaskStateChangeEventData : BaseEventData
    {
        public WorkTasks.WorkTask WorkTask { get; set; }
        public WorkTaskStatus OldWorkTaskStatus { get; set; }
        public WorkTaskStatus NewWorkTaskStatus { get; set; }
        public List<WorkTasks.WorkStep> WorkSteps { get; set; }
    }
}
