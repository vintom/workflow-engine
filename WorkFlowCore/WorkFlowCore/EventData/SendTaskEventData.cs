﻿using WorkFlowCore.Common.EventBus;
using System;
using System.Collections.Generic;
using System.Text;

namespace WorkFlowCore.EventData
{
    public class SendTaskEventData : BaseEventData
    {
        public WorkTasks.WorkTask WorkTask { get; set; }
        public List<WorkTasks.WorkStep> WorkSteps { get; set; }

    }
}
