﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkFlowCore.WorkTasks
{
    public static class WorkStepExtension
    {
        public static WorkStep ToWorkStep(this WorkStepInfo workStepInfo)
        {
            var workStep = new WorkStep();
            workStep.Comment = workStepInfo.Comment;
            workStep.CreationTime = workStepInfo.CreationTime;
            workStep.FormData = workStepInfo.FormData;
            workStep.FromNodeId = workStepInfo.FromNodeId;
            workStep.FromNodeName = workStepInfo.FromNodeName;
            workStep.GroupId = workStepInfo.GroupId;
            workStep.HandlerTime = workStepInfo.HandlerTime;
            workStep.HandleType = workStepInfo.HandleType;
            workStep.HandleUser = new UserSelectors.User(workStepInfo.HandleUser_Id, workStepInfo.HandleUser_Name); 
            workStep.Id = workStepInfo.Id;
            workStep.IsHandled = workStepInfo.IsHandled;
            workStep.IsRead = workStepInfo.IsRead;
            workStep.ModifiedTime = workStepInfo.ModifiedTime;
            workStep.ModifiedUserId = workStepInfo.ModifiedUserId;
            workStep.NodeId = workStepInfo.NodeId;
            workStep.NodeName = workStepInfo.NodeName;
            workStep.PreStepGroupId = workStepInfo.PreStepGroupId;
            workStep.ReadTime = workStepInfo.ReadTime;
            workStep.ResourceIds = workStepInfo.ResourceIds;
            workStep.WorkStepType = workStepInfo.WorkStepType;
            workStep.WorkTaskId = workStepInfo.WorkTaskId;
            workStep.FromForwardStepId = workStepInfo.FromForwardStepId;
            workStep.SubProcessNode = new SubProcessNode(workStepInfo.SubProcessNode_NodeId, workStepInfo.SubProcessNode_WorkStepId);

            workStep.CreatedUserId = workStepInfo.CreatedUserId;
            workStep.Deleted = workStepInfo.Deleted;
            workStep.DeletedUserId = workStepInfo.DeletedUserId;
            workStep.DeletedTime = workStepInfo.DeletedTime;

            return workStep;
        }

        public static WorkStepInfo ToWorkStepInfo(this WorkStep workStep, WorkStepInfo workStepInfo=null)
        {
            if(workStepInfo==null)
            workStepInfo = new WorkStepInfo();
            workStepInfo.Comment = workStep.Comment;
            workStepInfo.CreationTime = workStep.CreationTime;
            workStepInfo.FormData = workStep.FormData;
            workStepInfo.FromNodeId = workStep.FromNodeId;
            workStepInfo.FromNodeName = workStep.FromNodeName;
            workStepInfo.GroupId = workStep.GroupId;
            workStepInfo.HandlerTime = workStep.HandlerTime;
            workStepInfo.HandleType = workStep.HandleType;
            workStepInfo.HandleUser_Id = workStep.HandleUser?.Id;
            workStepInfo.HandleUser_Name = workStep.HandleUser?.Name;
            workStepInfo.Id = workStep.Id;
            workStepInfo.IsHandled = workStep.IsHandled;
            workStepInfo.IsRead = workStep.IsRead;
            workStepInfo.ModifiedTime = workStep.ModifiedTime;
            workStepInfo.ModifiedUserId = workStep.ModifiedUserId;
            workStepInfo.NodeId = workStep.NodeId;
            workStepInfo.NodeName = workStep.NodeName;
            workStepInfo.PreStepGroupId = workStep.PreStepGroupId;
            workStepInfo.ReadTime = workStep.ReadTime;
            workStepInfo.ResourceIds = workStep.ResourceIds;
            workStepInfo.WorkStepType = workStep.WorkStepType;
            workStepInfo.WorkTaskId = workStep.WorkTaskId;
            workStepInfo.FromForwardStepId = workStep.FromForwardStepId;
            workStepInfo.SubProcessNode_NodeId = workStep.SubProcessNode?.NodeId;
            workStepInfo.SubProcessNode_WorkStepId = workStep.SubProcessNode?.WorkStepId;


            workStepInfo.CreatedUserId = workStep.CreatedUserId;
            workStepInfo.Deleted = workStep.Deleted;
            workStepInfo.DeletedUserId = workStep.DeletedUserId;
            workStepInfo.DeletedTime = workStep.DeletedTime;

            return workStepInfo;
        }
    }
}
