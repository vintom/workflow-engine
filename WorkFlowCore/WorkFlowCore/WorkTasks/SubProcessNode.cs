﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkFlowCore.WorkTasks
{
    public class SubProcessNode
    {
        public SubProcessNode()
        {
        }

        public SubProcessNode(Guid? nodeId, Guid? workStepId)
        {
            NodeId = nodeId??Guid.Empty;
            WorkStepId = workStepId ?? Guid.Empty;
        }

        /// <summary>
        /// 子流程节点id
        /// </summary>
        public Guid NodeId { get; set; }
        /// <summary>
        /// 子流程节点处理id
        /// </summary>
        public Guid WorkStepId { get; set; }
    }
}
