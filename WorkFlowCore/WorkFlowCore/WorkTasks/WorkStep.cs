﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using WorkFlowCore.IRepositories;
using WorkFlowCore.UserSelectors;

namespace WorkFlowCore.WorkTasks
{
    public class WorkStep: WithBaseInfoEntity
    {
        public WorkStep()
        {
        }

        public WorkStep(Guid id, Guid workTaskId, Guid fromNodeId, string fromNodeName, Guid nodeId, string nodeName, User handleUser, WorkStepType workStepType, string groupId, string preStepGroupId)
        {
            Id = id;
            FromNodeId = fromNodeId;
            FromNodeName = fromNodeName;
            WorkTaskId = workTaskId;
            NodeId = nodeId;
            NodeName = nodeName;
            HandleUser = handleUser;
            WorkStepType = workStepType;
            GroupId = groupId;
            PreStepGroupId = preStepGroupId;
            HandleType = WorkStepHandleType.Processing;
        }

        /// <summary>
        /// 任务id
        /// </summary>
        public Guid WorkTaskId { get; set; }
        /// <summary>
        /// 阶段类型
        /// </summary>
        public WorkStepType WorkStepType { get; set; }
        /// <summary>
        /// 来源节点id
        /// </summary>
        public Guid FromNodeId { get; set; }
        public string FromNodeName { get; set; }
        /// <summary>
        /// 节点id
        /// </summary>
        public Guid NodeId { get; set; }
        public string NodeName { get; set; }
        /// <summary>
        /// 处理用户
        /// </summary>
        public User HandleUser { get; set; }
        /// <summary>
        /// 是否已处理
        /// </summary>
        public bool IsHandled { get; set; }
        /// <summary>
        /// 处理状态
        /// </summary>
        public WorkStepHandleType HandleType { get; set; }
        /// <summary>
        /// 批文
        /// </summary>
        public string Comment { get; set; }
        /// <summary>
        /// 附件资源id，多个之间  , 隔开
        /// </summary>
        public string ResourceIds { get; set; }
        /// <summary>
        /// 是否已读（已查看）
        /// </summary>
        public bool IsRead { get; set; }
        /// <summary>
        /// 阅读时间（第一次）
        /// </summary>
        public DateTime ReadTime { get; set; }
        /// <summary>
        /// 处理时间
        /// </summary>
        public DateTime HandlerTime { get; set; }
        /// <summary>
        /// 分组id 同一个阶段审批的节点分组id相同
        /// </summary>
        public string GroupId { get; set; }
        /// <summary>
        /// 前一个步骤的分组id
        /// </summary>
        public string PreStepGroupId { get; set; }
        /// <summary>
        /// 表单数据
        /// </summary>
        public string  FormData { get; set; }
        /// <summary>
        /// 子流程节点，为空，在作为子流程的审批时才会用到
        /// </summary>
        public SubProcessNode SubProcessNode { get; set; }

        /// <summary>
        /// 来源步骤id
        /// 如果是转发才会有值
        /// </summary>
        public Guid? FromForwardStepId { get; set; }

        public void FromForward(Guid stepId)
        {
            this.FromForwardStepId = stepId;
        }

        public void Readed()
        {
            IsRead = true;
            ReadTime = DateTime.Now;
        }

        public void Handle(WorkStepHandleType handleType, string comment=null, string resourceIds=null)
        {
            HandlerTime = DateTime.Now;
            if(comment!=null)
                Comment = comment;
            HandleType = handleType;
            if (resourceIds != null)
                ResourceIds = resourceIds;
            IsHandled = true;
            IsRead = true;
        }
        /// <summary>
        /// 以基本信息复制一个待处理的节点
        /// </summary>
        /// <returns></returns>
        public WorkStep Copy(string groupId=null)
        {
            var step = new WorkStep(Guid.NewGuid(),WorkTaskId, FromNodeId,FromNodeName, NodeId,NodeName, HandleUser, WorkStepType, groupId??GroupId, PreStepGroupId);
            step.SetFormData(FormData);
            return step;
        }
        public void SetReaded()
        {
            IsRead = true;
        }
        public void SetHandleUser(User user)
        {
            if (user != null)
                HandleUser = user;
        }
        public void SetFormData(string formData)
        {
            FormData = formData;
        }

        public void Work4SubProcess(SubProcessNode subProcessNode)
        {
            SubProcessNode = subProcessNode;
        }

    }
    /// <summary>
    /// 流程节点类型
    /// </summary>
    public enum WorkStepType
    {
        /// <summary>
        /// 处理（正常需要处理的情况）
        /// </summary>
        Handle,
        /// <summary>
        /// 只读（抄送情况下）
        /// </summary>
        ReadOnly,
        /// <summary>
        /// 自动处理
        /// </summary>
        Auto,
        
    }
    public enum WorkStepHandleType
    {
        /// <summary>
        /// 通过
        /// </summary>
        Pass,
        /// <summary>
        /// 拒绝
        /// </summary>
        Reject,
        /// <summary>
        /// 撤回
        /// </summary>
        Withdraw,
        /// <summary>
        /// 转发（转给某人审批）
        /// </summary>
        Forward,
        /// <summary>
        /// 未处理（在会签节点其它节点驳回，但是部分未处理的则更新为该状态）
        /// </summary>
        UnWork,
        /// <summary>
        /// 处理中（作为子流程审批步骤时才会）
        /// </summary>
        Processing,
    }
}
