﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using WorkFlowCore.IRepositories;
using WorkFlowCore.UserSelectors;

namespace WorkFlowCore.WorkTasks
{
    public class WorkStepInfo : WithBaseInfoEntity
    {
        public WorkStepInfo()
        {
        }

       

        /// <summary>
        /// 任务id
        /// </summary>
        public Guid WorkTaskId { get; set; }
        /// <summary>
        /// 阶段类型
        /// </summary>
        public WorkStepType WorkStepType { get; set; }
        /// <summary>
        /// 来源节点id
        /// </summary>
        public Guid FromNodeId { get; set; }
        public string FromNodeName { get; set; }
        /// <summary>
        /// 节点id
        /// </summary>
        public Guid NodeId { get; set; }
        public string NodeName { get; set; }
        /// <summary>
        /// 处理用户
        /// </summary>
        public string HandleUser_Id { get; set; }
        public string HandleUser_Name { get; set; }
        /// <summary>
        /// 是否已处理
        /// </summary>
        public bool IsHandled { get; set; }
        /// <summary>
        /// 处理状态
        /// </summary>
        public WorkStepHandleType HandleType { get; set; }
        /// <summary>
        /// 批文
        /// </summary>
        public string Comment { get; set; }
        /// <summary>
        /// 附件资源id，多个之间  , 隔开
        /// </summary>
        public string ResourceIds { get; set; }
        /// <summary>
        /// 是否已读（已查看）
        /// </summary>
        public bool IsRead { get; set; }
        /// <summary>
        /// 阅读时间（第一次）
        /// </summary>
        public DateTime ReadTime { get; set; }
        /// <summary>
        /// 处理时间
        /// </summary>
        public DateTime HandlerTime { get; set; }
        /// <summary>
        /// 分组id 同一个阶段审批的节点分组id相同
        /// </summary>
        public string GroupId { get; set; }
        /// <summary>
        /// 前一个步骤的分组id
        /// </summary>
        public string PreStepGroupId { get; set; }
        /// <summary>
        /// 表单数据
        /// </summary>
        public string FormData { get; set; }

        /// <summary>
        /// 来源步骤id
        /// 如果是转发才会有值
        /// </summary>
        public Guid? FromForwardStepId { get; set; }

        public Guid? SubProcessNode_NodeId { get; set; }
        public Guid? SubProcessNode_WorkStepId { get; set; }

    }
}
