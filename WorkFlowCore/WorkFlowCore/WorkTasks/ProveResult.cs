using System.Collections.Generic;

namespace WorkFlowCore.WorkTasks
{
    public class ProveResult
    {
        public ProveResultCode Code { get; set; }
        public string Msg { get; set; }
        public List<WorkStep> WorkSteps { get; set; }

        public static ProveResult Succeed(List<WorkStep> workSteps)
        {
            return new ProveResult
            {
                Code = ProveResultCode.SUCCESS,
                Msg = "成功",
                WorkSteps = workSteps
            };
        }

        public static ProveResult Failed(string msg)
        {
            return new ProveResult
            {
                Code =ProveResultCode.FAIL,
                Msg = msg,
            };
        }

        public enum ProveResultCode{
            SUCCESS,
            FAIL,
        }
    }
}