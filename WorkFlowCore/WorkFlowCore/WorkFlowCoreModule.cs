﻿using Microsoft.AspNetCore.Builder.Extensions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using Volo.Abp;
using Volo.Abp.Castle;
using Volo.Abp.EntityFrameworkCore;
using Volo.Abp.EventBus;
using Volo.Abp.Modularity;
using WorkFlowCore.Authorization;
using WorkFlowCore.Common.Authorization.JWT;
using WorkFlowCore.Conditions;
using WorkFlowCore.Plugins;
using WorkFlowCore.UserSelectors;
using WorkFlowCore.Workflows;
using WorkFlowCore.WorkTasks;

namespace WorkFlowCore
{
    [DependsOn(typeof(AbpEventBusModule))]
    [DependsOn(typeof(AbpCastleCoreModule))]
    [DependsOn(typeof(AbpEntityFrameworkCoreModule))]
    public  class WorkFlowCoreModule:AbpModule
    {
        public override void PreConfigureServices(ServiceConfigurationContext context)
        {
            base.PreConfigureServices(context);
        }

        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            base.ConfigureServices(context);

            var services = context.Services;
            services.AddScoped<UserSelectorManager>();
            services.AddScoped<ConditionManager>();

            services.AddScoped<Workflow>();
            services.AddScoped<WorkflowVersion>();
            services.AddScoped<WorkflowNode>();
            services.AddScoped<WorkflowLine>();
            services.AddScoped<WorkTask>();
            services.AddScoped<WorkStep>();
            services.AddScoped<WorkflowManager>();

            services.AddScoped<PluginManager>();

            services.AddScoped(typeof(IWorkflowSession), typeof(EmptySession));

            Configure<PluginOptions>(options =>
            {
                options.BasePath = Path.Combine(AppContext.BaseDirectory, "Plugins");
            });

            PluginManager.RegisterPluginType(typeof(ICondition), "条件处理器");
            PluginManager.RegisterPluginType(typeof(IUserSelector), "用户选择器");

            PluginManager.RegisterPluginType(typeof(ICustomizationVerifyExtension), "自定义认证器");
        }

        public override void OnApplicationInitialization(ApplicationInitializationContext context)
        {
            var pluginManager = context.ServiceProvider.GetService<PluginManager>();
            pluginManager.LoadPluginsRoot();
            base.OnApplicationInitialization(context);
        }
    }
}
