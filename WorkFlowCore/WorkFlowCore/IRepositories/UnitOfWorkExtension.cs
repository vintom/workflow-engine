﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Uow;

namespace WorkFlowCore.IRepositories
{
    public static class UnitOfWorkExtension
    {
        public static T Commit<T>(this IUnitOfWork unitOfWork, T succeedResult, T failedResult)
        {
            try
            {
                unitOfWork.SaveChangesAsync().Wait();
                return succeedResult;
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
                Console.Error.WriteLine(ex.ToString());
                return failedResult;
            }
        }

        public static bool Commit(this IUnitOfWork unitOfWork)
        {
            return Commit(unitOfWork, true, false);
        }
    }
}
