﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WorkFlowCore.WorkTasks;

namespace WorkFlowCore.IRepositories
{
    public interface IWorkTaskRepository: IBasicRepository<WorkTaskInfo, Guid>
    {
        Task<PageResult<WorkTask>> GetAllTasksOfUserAsync(string userId, int pageIndex = 1, int pageSize = -1);
        Task<PageResult<WorkTask>> GetWorkflowedTasksOfUserAsync(string userId, int pageIndex = 1, int pageSize = -1);
        Task<PageResult<WorkTask>> GetUnHandledWorkTasksOfUserAsync(string userId, int pageIndex = 1, int pageSize = -1);
        Task<PageResult<WorkTask>> GetHandledWorkTasksOfUserAsync(string userId, int pageIndex = 1, int pageSize = -1);
        Task<PageResult<WorkTask>> GetTasksOfStartUserAsync(string userId, int pageIndex = 1, int pageSize = -1);
        /// <summary>
        /// 获取用户发起的流程
        /// </summary>
        /// <param name="id"></param>
        /// <param name="currentPage"></param>
        /// <param name="maxResultCount"></param>
        /// <returns></returns>
        Task<PageResult<WorkTask>> GetWorkTasksOfCreatorAsync(string userId, int pageIndex = 1, int pageSize = -1);
        Task<PageResult<WorkTask>> GetAllWorkTasksAsync(int pageIndex = 1, int pageSize = -1);
    }
}
