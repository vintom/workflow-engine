﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WorkFlowCore.Workflows;
using WorkFlowCore.WorkTasks;

namespace WorkFlowCore.IRepositories
{
    public interface IWorkflowRepository : IBasicRepository<Workflow, Guid>
    {
        Task<List<Workflow>> GetAllWorkflowsWithVersion();
    }
}
