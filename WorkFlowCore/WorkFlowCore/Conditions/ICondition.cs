﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkFlowCore.Conditions
{
    public interface ICondition
    {
        bool CanAccept(ConditionInput input);
    }
}
