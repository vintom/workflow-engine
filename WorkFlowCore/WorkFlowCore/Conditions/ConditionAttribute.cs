﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkFlowCore.Conditions
{
    public class ConditionAttribute: Attribute
    {
        public string Name;
        public string Description;

        public ConditionAttribute(string name, string description=null)
        {
            Name = name;
            Description = description;
        }
    }
}
