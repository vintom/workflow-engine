﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkFlowCore.FormDesigns
{
    public class WorkflowId4FormDesign
    {
        public WorkflowId4FormDesign()
        {
        }

        public WorkflowId4FormDesign(Guid workflowId, int version)
        {
            Id = workflowId;
            Version = version;
        }

        public Guid Id { get; set; }
        public int Version { get; set; }
    }
}
