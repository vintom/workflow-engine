﻿using System;
using System.Collections.Generic;
using System.Text;
using WorkFlowCore.IRepositories;

namespace WorkFlowCore.FormDesigns
{
    public class FormDesignInfo : WithBaseInfoEntity
    {
        public FormDesignInfo()
        {
        }



        public string FormType { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int? ActiveVersion { get; set; }
        public Guid WorkflowId_Id { get; set; }
        public int WorkflowId_Version { get; set; }

    }
}
