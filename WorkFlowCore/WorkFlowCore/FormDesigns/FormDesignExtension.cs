﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkFlowCore.FormDesigns
{
    public static class FormDesignExtension
    {
        public static FormDesign ToFormDesign(this FormDesignInfo formDesignInfo)
        {
            var formDesign = new FormDesign();


            formDesign.WorkflowId = new WorkflowId4FormDesign(formDesignInfo.WorkflowId_Id, formDesignInfo.WorkflowId_Version);
            formDesign.FormType = formDesignInfo.FormType;
            formDesign.Title = formDesignInfo.Title;
            formDesign.Description = formDesignInfo.Description;
            formDesign.ActiveVersion = formDesignInfo.ActiveVersion;


            formDesign.ModifiedTime = formDesignInfo.ModifiedTime;
            formDesign.ModifiedUserId = formDesignInfo.ModifiedUserId;
            formDesign.CreationTime = formDesignInfo.CreationTime;
            formDesign.CreatedUserId = formDesignInfo.CreatedUserId;
            formDesign.Deleted = formDesignInfo.Deleted;
            formDesign.DeletedUserId = formDesignInfo.DeletedUserId;
            formDesign.DeletedTime = formDesignInfo.DeletedTime;
            formDesign.Id = formDesignInfo.Id;

            return formDesign;
        }

        public static FormDesignInfo ToFormDesignInfo(this FormDesign formDesign, FormDesignInfo formDesignInfo = null)
        {
            if (formDesignInfo == null)
                formDesignInfo = new FormDesignInfo();


            if (formDesign.WorkflowId != null)
            {
                formDesignInfo.WorkflowId_Id = formDesign.WorkflowId.Id;
                formDesignInfo.WorkflowId_Version = formDesign.WorkflowId.Version;
            }


            formDesignInfo.FormType = formDesign.FormType;
            formDesignInfo.Title = formDesign.Title;
            formDesignInfo.Description = formDesign.Description;
            formDesignInfo.ActiveVersion = formDesign.ActiveVersion;


            formDesignInfo.ModifiedTime = formDesign.ModifiedTime;
            formDesignInfo.ModifiedUserId = formDesign.ModifiedUserId;
            formDesignInfo.CreatedUserId = formDesign.CreatedUserId;
            formDesignInfo.Deleted = formDesign.Deleted;
            formDesignInfo.DeletedUserId = formDesign.DeletedUserId;
            formDesignInfo.DeletedTime = formDesign.DeletedTime;
            formDesignInfo.Id = formDesign.Id;

            return formDesignInfo;
        }
    }
}
