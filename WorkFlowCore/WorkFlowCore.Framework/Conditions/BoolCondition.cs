﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using WorkFlowCore.Conditions;

namespace WorkFlowCore.Framework.Conditions
{
    [Condition("布尔处理器","参数传 true/false")]
    public class BoolCondition : ICondition
    {
        public bool CanAccept(ConditionInput input)
        {
            try
            {
                return input.Expression?.Trim().ToLower() == "true";
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
