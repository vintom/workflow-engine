﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkFlowCore.Conditions;
using WorkFlowCore.Plugins;

namespace WorkFlowCore.Framework.Conditions
{
    [Condition("可配置条件处理器")]
    public class ConfigurableConditionDemo : ICondition, IConfigurable
    {
        public bool CanAccept(ConditionInput input)
        {
            return "true" == (Parameters?.GetValueOrDefault("value", "false") ?? "false");
        }

        private Dictionary<string, string> Parameters;
        public async Task Configure(ConfigureContext configureContext)
        {
            Parameters = configureContext.Parameters;
        }
    }
}
