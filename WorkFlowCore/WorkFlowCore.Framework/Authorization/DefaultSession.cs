﻿using IdentityModel;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using WorkFlowCore.Authorization;
using WorkFlowCore.Common.Authentication.JWT;
using WorkFlowCore.Framework.UserSelectors;
using WorkFlowCore.UserSelectors;

namespace WorkFlowCore.Framework.Authorization
{
    public class DefaultSession : IWorkflowSession
    {
        public DefaultSession(IHttpContextAccessor httpContextAccessor)
        {
            _User = new EmptyUser();
            this.httpContextAccessor = httpContextAccessor;
        }

        private User _User;
        private readonly IHttpContextAccessor httpContextAccessor;

        public User User
        {
            get
            {
                if (_User is EmptyUser)
                {
                    _User = GetUser();
                }
                return _User;
            }
        }
        public bool IsManager {
            get 
            {
                if (httpContextAccessor.HttpContext == null) return false;

                var authenticate = httpContextAccessor.HttpContext.AuthenticateAsync().Result;
                if (authenticate.Succeeded)
                {
                    //这里，如果是通过 管理员账号登录，则从头部 user-info 获取代理的 用户信息，否则，从token凭据中的 user-info 解析用户信息
                    var IsManager = authenticate.Principal.Claims.Any(c => c.Type == "ismanager" && c.Value == JWTAuthenticationOptions.ClaimTypesRole_Admin);
                    return IsManager;
                }
                return false;
            }
        }

       

        private User GetUser()
        {
            if (httpContextAccessor.HttpContext == null) new EmptyUser();

            var authenticate = httpContextAccessor.HttpContext.AuthenticateAsync().Result;
            if (authenticate.Succeeded)
            {
                if (IsManager)
                {
                    try
                    {

                        var userInfoStr = httpContextAccessor.HttpContext.Request.Headers["user-info"];
                        userInfoStr = WebUtility.UrlDecode(userInfoStr);
                        return JsonConvert.DeserializeObject<User>(userInfoStr);
                    }
                    catch (Exception)
                    {
                        return new EmptyUser();
                    }
                }
                else
                {
                    try
                    {

                        var userInfoStr = authenticate.Principal.Claims.FirstOrDefault(c => c.Type == "user-info").Value;
                        userInfoStr = WebUtility.UrlDecode(userInfoStr);
                        return JsonConvert.DeserializeObject<User>(userInfoStr);
                    }
                    catch (Exception)
                    {
                        //this.User = new User(user.Id, user.Name);
                    }
                }

            }
            return new EmptyUser();
        }
    }
}
