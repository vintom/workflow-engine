﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkFlowCore.Framework
{
    internal class WorkFlowCoreFrameworkOptions
    {
        public static FrameworkConfigOrmType DbType { get; set; } = FrameworkConfigOrmType.LocalMemory;
    }

    internal enum FrameworkConfigOrmType
    {
        LocalMemory,
        MySql
    }
}
