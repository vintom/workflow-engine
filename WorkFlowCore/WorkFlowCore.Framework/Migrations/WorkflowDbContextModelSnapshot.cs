﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Volo.Abp.EntityFrameworkCore;
using WorkFlowCore.Framework.Repositories4EF;

#nullable disable

namespace WorkFlowCore.Framework.Migrations
{
    [DbContext(typeof(WorkflowDbContext))]
    partial class WorkflowDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("_Abp_DatabaseProvider", EfCoreDatabaseProvider.MySql)
                .HasAnnotation("ProductVersion", "6.0.5")
                .HasAnnotation("Relational:MaxIdentifierLength", 64);

            modelBuilder.Entity("WorkFlowCore.FormDesigns.FormDesignInfo", b =>
                {
                    b.Property<Guid>("Id")
                        .HasColumnType("char(36)");

                    b.Property<int?>("ActiveVersion")
                        .HasColumnType("int");

                    b.Property<string>("CreatedUserId")
                        .HasColumnType("longtext");

                    b.Property<DateTime>("CreationTime")
                        .HasColumnType("datetime(6)");

                    b.Property<bool>("Deleted")
                        .HasColumnType("bit(1)");

                    b.Property<DateTime>("DeletedTime")
                        .HasColumnType("datetime(6)");

                    b.Property<string>("DeletedUserId")
                        .HasColumnType("longtext");

                    b.Property<string>("Description")
                        .HasColumnType("longtext");

                    b.Property<string>("FormType")
                        .HasColumnType("longtext");

                    b.Property<DateTime>("ModifiedTime")
                        .HasColumnType("datetime(6)");

                    b.Property<string>("ModifiedUserId")
                        .HasColumnType("longtext");

                    b.Property<string>("Title")
                        .HasColumnType("longtext");

                    b.Property<Guid>("WorkflowId_Id")
                        .HasColumnType("char(36)");

                    b.Property<int>("WorkflowId_Version")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.ToTable("FormDesignInfos");
                });

            modelBuilder.Entity("WorkFlowCore.FormDesigns.FormDesignVersion", b =>
                {
                    b.Property<Guid>("Id")
                        .HasColumnType("char(36)");

                    b.Property<string>("CreatedUserId")
                        .HasColumnType("longtext");

                    b.Property<DateTime>("CreationTime")
                        .HasColumnType("datetime(6)");

                    b.Property<bool>("Deleted")
                        .HasColumnType("bit(1)");

                    b.Property<DateTime>("DeletedTime")
                        .HasColumnType("datetime(6)");

                    b.Property<string>("DeletedUserId")
                        .HasColumnType("longtext");

                    b.Property<string>("DesignContent")
                        .HasColumnType("longtext");

                    b.Property<Guid>("FormDesignId")
                        .HasColumnType("char(36)");

                    b.Property<DateTime>("ModifiedTime")
                        .HasColumnType("datetime(6)");

                    b.Property<string>("ModifiedUserId")
                        .HasColumnType("longtext");

                    b.Property<int>("Version")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.ToTable("FormDesignVersions");
                });

            modelBuilder.Entity("WorkFlowCore.Plugins.PluginApplyConfig", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken()
                        .HasMaxLength(40)
                        .HasColumnType("varchar(40)")
                        .HasColumnName("ConcurrencyStamp");

                    b.Property<string>("ConfigValue")
                        .HasColumnType("longtext");

                    b.Property<string>("Description")
                        .HasColumnType("longtext");

                    b.Property<bool>("Enabled")
                        .HasColumnType("bit(1)");

                    b.Property<string>("EntryFullName")
                        .HasColumnType("longtext");

                    b.Property<string>("ExecutableTypeFullName")
                        .HasColumnType("longtext");

                    b.Property<string>("ExtraProperties")
                        .HasColumnType("longtext")
                        .HasColumnName("ExtraProperties");

                    b.Property<string>("Name")
                        .HasColumnType("longtext");

                    b.Property<int>("Order")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.ToTable("PluginApplyConfigs");
                });

            modelBuilder.Entity("WorkFlowCore.Workflows.Workflow", b =>
                {
                    b.Property<Guid>("Id")
                        .HasColumnType("char(36)");

                    b.Property<int>("ActiveVersion")
                        .HasColumnType("int");

                    b.Property<string>("CreatedUserId")
                        .HasColumnType("longtext");

                    b.Property<DateTime>("CreationTime")
                        .HasColumnType("datetime(6)");

                    b.Property<bool>("Deleted")
                        .HasColumnType("bit(1)");

                    b.Property<DateTime>("DeletedTime")
                        .HasColumnType("datetime(6)");

                    b.Property<string>("DeletedUserId")
                        .HasColumnType("longtext");

                    b.Property<string>("Description")
                        .HasMaxLength(500)
                        .HasColumnType("varchar(500)");

                    b.Property<bool>("IsLock")
                        .HasColumnType("bit(1)");

                    b.Property<DateTime>("ModifiedTime")
                        .HasColumnType("datetime(6)");

                    b.Property<string>("ModifiedUserId")
                        .HasColumnType("longtext");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("varchar(50)");

                    b.Property<string>("WorkflowNo")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("varchar(50)");

                    b.HasKey("Id");

                    b.ToTable("Workflows");
                });

            modelBuilder.Entity("WorkFlowCore.Workflows.WorkflowVersionInfo", b =>
                {
                    b.Property<Guid>("Id")
                        .HasColumnType("char(36)");

                    b.Property<string>("AllNodes")
                        .HasColumnType("longtext");

                    b.Property<string>("CreatedUserId")
                        .HasColumnType("longtext");

                    b.Property<DateTime>("CreationTime")
                        .HasColumnType("datetime(6)");

                    b.Property<bool>("Deleted")
                        .HasColumnType("bit(1)");

                    b.Property<DateTime>("DeletedTime")
                        .HasColumnType("datetime(6)");

                    b.Property<string>("DeletedUserId")
                        .HasColumnType("longtext");

                    b.Property<string>("Description")
                        .HasMaxLength(2000)
                        .HasColumnType("varchar(2000)");

                    b.Property<string>("DrawingInfo")
                        .HasColumnType("longtext");

                    b.Property<bool>("IsLock")
                        .HasColumnType("bit(1)");

                    b.Property<DateTime>("ModifiedTime")
                        .HasColumnType("datetime(6)");

                    b.Property<string>("ModifiedUserId")
                        .HasColumnType("longtext");

                    b.Property<string>("NodeMaps")
                        .HasColumnType("longtext");

                    b.Property<int>("VersionNo")
                        .HasColumnType("int");

                    b.Property<Guid>("WorkflowId")
                        .HasColumnType("char(36)");

                    b.HasKey("Id");

                    b.ToTable("WorkflowVersionInfos");
                });

            modelBuilder.Entity("WorkFlowCore.WorkTasks.WorkStepInfo", b =>
                {
                    b.Property<Guid>("Id")
                        .HasColumnType("char(36)");

                    b.Property<string>("Comment")
                        .HasColumnType("longtext");

                    b.Property<string>("CreatedUserId")
                        .HasColumnType("longtext");

                    b.Property<DateTime>("CreationTime")
                        .HasColumnType("datetime(6)");

                    b.Property<bool>("Deleted")
                        .HasColumnType("bit(1)");

                    b.Property<DateTime>("DeletedTime")
                        .HasColumnType("datetime(6)");

                    b.Property<string>("DeletedUserId")
                        .HasColumnType("longtext");

                    b.Property<string>("FormData")
                        .HasColumnType("longtext");

                    b.Property<Guid?>("FromForwardStepId")
                        .HasColumnType("char(36)");

                    b.Property<Guid>("FromNodeId")
                        .HasColumnType("char(36)");

                    b.Property<string>("FromNodeName")
                        .HasColumnType("longtext");

                    b.Property<string>("GroupId")
                        .HasColumnType("longtext");

                    b.Property<int>("HandleType")
                        .HasColumnType("int");

                    b.Property<string>("HandleUser_Id")
                        .HasColumnType("longtext");

                    b.Property<string>("HandleUser_Name")
                        .HasColumnType("longtext");

                    b.Property<DateTime>("HandlerTime")
                        .HasColumnType("datetime(6)");

                    b.Property<bool>("IsHandled")
                        .HasColumnType("bit(1)");

                    b.Property<bool>("IsRead")
                        .HasColumnType("bit(1)");

                    b.Property<DateTime>("ModifiedTime")
                        .HasColumnType("datetime(6)");

                    b.Property<string>("ModifiedUserId")
                        .HasColumnType("longtext");

                    b.Property<Guid>("NodeId")
                        .HasColumnType("char(36)");

                    b.Property<string>("NodeName")
                        .HasColumnType("longtext");

                    b.Property<string>("PreStepGroupId")
                        .HasColumnType("longtext");

                    b.Property<DateTime>("ReadTime")
                        .HasColumnType("datetime(6)");

                    b.Property<string>("ResourceIds")
                        .HasColumnType("longtext");

                    b.Property<Guid?>("SubProcessNode_NodeId")
                        .HasColumnType("char(36)");

                    b.Property<Guid?>("SubProcessNode_WorkStepId")
                        .HasColumnType("char(36)");

                    b.Property<int>("WorkStepType")
                        .HasColumnType("int");

                    b.Property<Guid>("WorkTaskId")
                        .HasColumnType("char(36)");

                    b.HasKey("Id");

                    b.ToTable("WorkStepInfos");
                });

            modelBuilder.Entity("WorkFlowCore.WorkTasks.WorkTaskInfo", b =>
                {
                    b.Property<Guid>("Id")
                        .HasColumnType("char(36)");

                    b.Property<string>("CreatedUserId")
                        .HasColumnType("longtext");

                    b.Property<DateTime>("CreationTime")
                        .HasColumnType("datetime(6)");

                    b.Property<bool>("Deleted")
                        .HasColumnType("bit(1)");

                    b.Property<DateTime>("DeletedTime")
                        .HasColumnType("datetime(6)");

                    b.Property<string>("DeletedUserId")
                        .HasColumnType("longtext");

                    b.Property<string>("EntityFullName")
                        .HasColumnType("longtext");

                    b.Property<string>("EntityKeyValue")
                        .HasColumnType("longtext");

                    b.Property<string>("FormData")
                        .HasColumnType("longtext");

                    b.Property<bool>("IsSimulation")
                        .HasColumnType("bit(1)");

                    b.Property<DateTime>("ModifiedTime")
                        .HasColumnType("datetime(6)");

                    b.Property<string>("ModifiedUserId")
                        .HasColumnType("longtext");

                    b.Property<string>("Name")
                        .HasColumnType("longtext");

                    b.Property<int>("WorkTaskStatus")
                        .HasColumnType("int");

                    b.Property<Guid>("WorkflowId_Id")
                        .HasColumnType("char(36)");

                    b.Property<int>("WorkflowId_VersionId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.ToTable("WorkTaskInfos");
                });
#pragma warning restore 612, 618
        }
    }
}
