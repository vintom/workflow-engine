﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WorkFlowCore.Framework.Migrations
{
    public partial class addfields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsSimulation",
                table: "WorkTaskInfos",
                type: "bit(1)",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsSimulation",
                table: "WorkTaskInfos");
        }
    }
}
