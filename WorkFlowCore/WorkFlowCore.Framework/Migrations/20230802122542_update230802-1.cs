﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WorkFlowCore.Framework.Migrations
{
    public partial class update2308021 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsLock",
                table: "Workflows",
                type: "bit(1)",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsLock",
                table: "Workflows");
        }
    }
}
