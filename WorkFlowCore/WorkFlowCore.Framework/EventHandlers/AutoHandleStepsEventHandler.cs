﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;
using Volo.Abp.EventBus;
using WorkFlowCore.Common.EventBus;
using WorkFlowCore.EventData;
using WorkFlowCore.WorkTasks;

namespace WorkFlowCore.Framework.EventHandlers
{
    public class AutoHandleStepsEventHandler : ILocalEventHandler<AutoHandleStepsEventData>,
          ITransientDependency
    {
        private readonly WorkTaskManager workTaskManager;

        public AutoHandleStepsEventHandler(WorkTaskManager workTaskManager)
        {
            this.workTaskManager = workTaskManager;
        }

        public void Handle(AutoHandleStepsEventData data)
        {
            if (data == null || data.Steps == null) return;
            foreach (var step in data.Steps)
            {
                workTaskManager.PassApprove(step.Id, data.Comment, string.Empty).Wait();
            }
        }

        public Task HandleEventAsync(AutoHandleStepsEventData eventData)
        {
            Handle(eventData); 
            return Task.CompletedTask;
        }
    }
}
