﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;
using Volo.Abp.EventBus;
using WorkFlowCore.Common.EventBus;
using WorkFlowCore.EventData;
using WorkFlowCore.Plugins;
using WorkFlowCore.WorkTasks;

namespace WorkFlowCore.Framework.EventHandlers
{
    public class TaskStateChangeEventEventHandler : ILocalEventHandler<TaskStateChangeEventData>, ITransientDependency
    {
        private readonly PluginManager pluginManager;

        public TaskStateChangeEventEventHandler(PluginManager pluginManager)
        {
            this.pluginManager = pluginManager;
        }

        public async Task HandleEventAsync(TaskStateChangeEventData eventData)
        {
            var handlers = pluginManager.ResolveAll<ICustomTaskStateChangeEventEventHandler>();
            if(handlers != null)
            {
                foreach (var handler in handlers)
                {
                    try
                    {
                        await handler.HandleEventAsync(eventData);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                    }
                }
            }
            await Task.CompletedTask;
        }
    }


    public interface ICustomTaskStateChangeEventEventHandler:IConfigurable
    {
        Task HandleEventAsync(TaskStateChangeEventData eventData);
    }


}
