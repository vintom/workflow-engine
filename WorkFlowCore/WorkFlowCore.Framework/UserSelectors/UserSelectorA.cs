﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WorkFlowCore.UserSelectors;

namespace WorkFlowCore.Framework.UserSelectors
{
    [UserSelector("按规则选择")]
    public class UserSelectorA : IUserSelector
    {

        public List<Selection> GetSelections()
        {
            return new List<Selection>()
            {
                new Selection{ Id="creator", Name="创建人"},
                new Selection{ Id="leader", Name="上级领导"},
                new Selection{ Id="boss", Name="总经理"}
            };
        }

        public List<User> GetUsers(SelectorInput input)
        {
            var result = new List<User>();
            try
            {
                switch (input.SelectionId)
                {
                    case "creator":
                        {
                            var user = UserList.GetUserById(input.WorkTask?.CreatedUserId);
                            result.Add(new User { Id = user?.Id, Name = user?.Name });
                            break;
                        }

                    case "leader":
                        {
                            var user = UserList.GetLeaderById(input.WorkTask?.CreatedUserId);
                            result.Add(new User { Id = user?.Id, Name = user?.Name });
                            break;
                        }

                    case "boss":
                        {
                            var user = UserList.Users.First();
                            result.Add(new User { Id = user?.Id, Name = user?.Name });
                            break;
                        }
                    default:
                        result.Add(new User { Id = input.SelectionId, Name = input.SelectionId });
                        break;
                }
            }
            catch (Exception)
            {

            }
            return result;
        }
    }
}
