﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace WorkFlowCore.Framework.Repositories4EF
{
    public class WorkflowDbContextFactory : IDesignTimeDbContextFactory<WorkflowDbContext>
    {
        public WorkflowDbContext CreateDbContext(string[] args)
        {
            var configuration = BuildConfiguration();
            configuration.GetCustomConnectionString();
            var builder = new DbContextOptionsBuilder<WorkflowDbContext>()
                .UseMySql(configuration.GetValueFromManyChanels("ConnectionStrings:Default"),new MySqlServerVersion(configuration.GetConnectionString("DefaultVersion")));

            return new WorkflowDbContext(builder.Options);
        }

        private static IConfigurationRoot BuildConfiguration()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false);

            return builder.Build();
        }
    }
}
