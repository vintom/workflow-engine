﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.EntityFrameworkCore;
using WorkFlowCore.Authorization;
using WorkFlowCore.IRepositories;
using WorkFlowCore.Workflows;
using WorkFlowCore.WorkTasks;

namespace WorkFlowCore.Framework.Repositories4EF
{
    public class WorkflowRepository : BasicRepository4EF<Workflow, Guid>, IWorkflowRepository
    {
        public WorkflowRepository(IWorkflowSession session,IDbContextProvider<WorkflowDbContext> dbContextProvider) : base(session,dbContextProvider)
        {
        }

        public async Task<List<Workflow>> GetAllWorkflowsWithVersion()
        {
            var result = new List<Workflow>();
            var workflows = await DbContext.Set<Workflow>().Where(w => !w.Deleted).ToListAsync();
            var versionIds = workflows.Select(w => w.Id);
            var allVersions = await DbContext.Set<WorkflowVersionInfo>().Where(v => versionIds.Contains(v.WorkflowId)).Select(v =>new {v.WorkflowId,v.VersionNo}).ToListAsync();


            workflows.ForEach(w =>
            {
                var versions = allVersions.Where(v => v.WorkflowId == w.Id);
                result.AddRange(versions.Select(v => new Workflow
                {
                    Id = w.Id,
                    WorkflowNo = w.WorkflowNo,
                    Name = w.Name,
                    ActiveVersion = v.VersionNo
                }));
            });
            return result.OrderBy(w=>w.Name).ToList();
        }
    }
}
