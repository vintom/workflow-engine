﻿using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Data;
using Volo.Abp.EntityFrameworkCore;
using WorkFlowCore.FormDesigns;
using WorkFlowCore.Plugins;
using WorkFlowCore.Workflows;
using WorkFlowCore.WorkTasks;

namespace WorkFlowCore.Framework.Repositories4EF
{
    public class WorkflowDbContext : AbpDbContext<WorkflowDbContext>
    {
        public WorkflowDbContext([NotNull] DbContextOptions<WorkflowDbContext> options) : base(options)
        {
        }

        public DbSet<Workflow> Workflows { get; set; }
        public DbSet<WorkflowVersionInfo> WorkflowVersionInfos { get; set; }
        public DbSet<WorkStepInfo> WorkStepInfos { get; set; }
        public DbSet<WorkTaskInfo> WorkTaskInfos { get; set; }


        //表单设计管理
        public DbSet<FormDesignInfo> FormDesignInfos { get; set; }
        public DbSet<FormDesignVersion> FormDesignVersions { get; set; }

        //插件管理
        public DbSet<PluginApplyConfig> PluginApplyConfigs { get; set; }
    }
}
