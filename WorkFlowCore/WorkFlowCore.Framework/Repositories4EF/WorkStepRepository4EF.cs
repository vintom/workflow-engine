﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.EntityFrameworkCore;
using WorkFlowCore.Authorization;
using WorkFlowCore.IRepositories;
using WorkFlowCore.WorkTasks;

namespace WorkFlowCore.Framework.Repositories4EF
{
    public class WorkStepRepository4EF : BasicRepository4EF<WorkStepInfo, Guid>, IWorkStepRepository
    {
        public WorkStepRepository4EF(IWorkflowSession session,IDbContextProvider<WorkflowDbContext> dbContextProvider) : base(session,dbContextProvider)
        {
        }

        public async Task<PageResult<WorkStep>> GetUnHandlerWorkStepsOfUserAsync(string userId, int pageIndex = 1, int pageSize = -1)
        {
            var result = new PageResult<WorkStep>
            {
                Total = (await GetCountAsync(ws => ws.HandleUser_Id == userId && !ws.IsHandled))
            };
            if (pageSize < 1)
                result.Items = (await GetListAsync(ws => ws.HandleUser_Id == userId && !ws.IsHandled)).Select(w => w.ToWorkStep()).ToList();
            else result.Items = (await GetPagedListAsync(ws => ws.HandleUser_Id == userId && !ws.IsHandled,(pageIndex-1)*pageSize,pageSize,"createdtime desc")).Select(w => w.ToWorkStep()).ToList();
            return await Task.FromResult(result);
        }
    }
}
