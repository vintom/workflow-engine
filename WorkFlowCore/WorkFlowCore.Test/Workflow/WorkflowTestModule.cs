﻿using Autofac.Core;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Modularity;
using WorkFlowCore.Conditions;
using WorkFlowCore.EntityFrameworkCore;
using WorkFlowCore.Framework;
using WorkFlowCore.UserSelectors;

namespace WorkFlowCore.Test.Workflow
{
    [DependsOn(typeof(WorkflowEntityFrameworkCoreTestModule))]
    public class WorkflowTestModule:AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            base.ConfigureServices(context);

            var services = context.Services;

            var assembly = Assembly.GetExecutingAssembly();

            //项目实现的
            UserSelectorManager.RegisterSelector(services, assembly);
            ConditionManager.Registercondition(services, assembly);

            

        }
    }
}
