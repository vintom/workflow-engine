﻿using Autofac.Core;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Modularity;
using WorkFlowCore.Conditions;
using WorkFlowCore.EntityFrameworkCore;
using WorkFlowCore.Framework;
using WorkFlowCore.Plugins;
using WorkFlowCore.UserSelectors;

namespace WorkFlowCore.Test.Plugins
{
    [DependsOn(typeof(WorkflowEntityFrameworkCoreTestModule))]
    public class PluginTestModule:AbpModule
    {
        public override void PreConfigureServices(ServiceConfigurationContext context)
        {
            

            base.PreConfigureServices(context);

        }
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            base.ConfigureServices(context);
            

        }
    }
}
