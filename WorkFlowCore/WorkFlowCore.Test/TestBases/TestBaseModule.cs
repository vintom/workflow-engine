﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Autofac;
using Volo.Abp.Modularity;
using Volo.Abp.Threading;
using Volo.Abp;
using Autofac.Core;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace WorkFlowCore.Test.TestBases
{

    [DependsOn(
        typeof(AbpAutofacModule),
        typeof(AbpTestBaseModule)
        )]
    public class TestBaseModule : AbpModule
    {
        public override void PreConfigureServices(ServiceConfigurationContext context)
        {

        }

        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
        }

        public override void OnApplicationInitialization(ApplicationInitializationContext context)
        {

        }
    }
}
