﻿using Volo.Abp;
using WorkFlowCore.Test.TestBases;

namespace WorkFlowCore.EntityFrameworkCore;

public abstract class WorkflowEntityFrameworkCoreTestBase : TestBase<WorkflowEntityFrameworkCoreTestModule>
{

}
